// grab the nerd model we just created
var Nerd = require('./models/nerd');
var User = require('./models/user');

    module.exports = function(app) {

        // server routes ===========================================================
        // handle things like api calls
        // authentication routes
        
        // middleware to use for all requests
        app.use(function(req, res, next) {
            // do logging
            console.log('Something is happening.');
            next(); // make sure we go to the next routes and don't stop here
        });

        // get all nerds in the database (accessed at GET https://lab4-roger13.c9users.io:8080/api/nerds)
        app.get('/api/nerds', function(req, res) {

            // use mongoose to get all nerds in the database
            Nerd.find(function(err, nerds) {
            
                // if there is an error retrieving, send the error. 
                                // nothing after res.send(err) will execute
                if (err) {
                    res.send(err);
                    return;
                }

                res.json(nerds); // return all nerds in JSON format
            });
        });
        
        // get all users in the database (accessed at GET https://lab4-roger13.c9users.io:8080/api/users)
        app.get('/api/users', function(req, res) {

            // use mongoose to get all nerds in the database
            User.find(function(err, users) {
            
                // if there is an error retrieving, send the error. 
                                // nothing after res.send(err) will execute
                if (err) {
                    res.send(err);
                    return;
                }

                res.json(users); // return all nerds in JSON format
            });
        });

        // create a nerd (accessed at POST https://lab4-roger13.c9users.io:8080/api/nerds)
        app.post('/api/nerds', function(req, res) {
            
            var nerd = new Nerd();      // create a new instance of the Nerd model
            nerd.text = req.body.text;  // set the nerds name (comes from the request)
    
            // save the nerd and check for errors
            nerd.save(function(err) {
                if (err) {
                    res.send(err);
                    return;
                }
    
                res.json({ message: 'Post created!' });
            });
            
        });
        
        // create a user (accessed at POST https://lab4-roger13.c9users.io:8080/api/users)
        app.post('/api/users', function(req, res) {
            
            var user = new User();      // create a new instance of the User model
            user.email = req.body.email; 
            user.id = req.body.id;  
            user.gender = req.body.gender;  
            user.name = req.body.name;  
    
            // save the user and check for errors
            user.save(function(err) {
                if (err) {
                    res.send(err);
                    return;
                }
    
                res.json({ message: 'User created!' });
            });
            
        });
        
        // get the nerd with that id (accessed at GET https://lab4-roger13.c9users.io:8080/api/nerds/:nerd_id)
        app.get('/api/nerds/:nerd_id', function(req, res) {
                Nerd.findById(req.params.nerd_id, function(err, nerd) {
                    if (err) {
                        res.send(err);
                        return;
                    }
                    res.json(nerd);
                });
        });
        
        // update the nerd with this id (accessed at PUT https://lab4-roger13.c9users.io:8080/api/nerds/:nerd_id)
        app.put('/api/nerds/:nerd_id', function(req, res) {

            // use our nerd model to find the nerd we want
            Nerd.findById(req.params.nerd_id, function(err, nerd) {
    
                if (err) {
                    res.send(err);
                    return;
                }
    
                nerd.text = req.body.text;  // update the nerds info
    
                // save the nerd
                nerd.save(function(err) {
                    if (err) {
                        res.send(err);
                        return;
                    }
    
                    res.json({ message: 'Post updated!' });
                });
    
            });
        });
        
        // update the user with this id (accessed at PUT https://lab4-roger13.c9users.io:8080/api/users/:user_id)
        app.put('/api/users/:user_id', function(req, res) {

            // use our nerd model to find the nerd we want
            User.findById(req.params.user_id, function(err, user) {
    
                if (err) {
                    res.send(err);
                    return;
                }
    
                user.name = req.body.name;  // update the user info
                user.id = req.body.id;
                user.email = req.body.email;
                user.gender = req.body.gender;
                
                // save the nerd
                user.save(function(err) {
                    if (err) {
                        res.send(err);
                        return;
                    }
    
                    res.json({ message: 'Post updated!' });
                });
    
            });
        });
        
        // delete the nerd with this id (accessed at DELETE https://lab4-roger13.c9users.io:8080/api/nerds/:nerd_id)
        app.delete('/api/nerds/:nerd_id', function(req, res) {
            Nerd.remove({_id: req.params.nerd_id}, function(err, nerd) {
                if (err) {
                    res.send(err);
                    return;
                }
    
                res.json({ message: 'Successfully deleted' });
            });
        });
        
        // delete the user with this id (accessed at DELETE https://lab4-roger13.c9users.io:8080/api/users/:user_id)
        app.delete('/api/users/:user_id', function(req, res) {
            User.remove({_id: req.params.user_id}, function(err, user) {
                if (err) {
                    res.send(err);
                    return;
                }
    
                res.json({ message: 'Successfully deleted' });
            });
        });
        
        /*
        // frontend routes =========================================================
        // route to handle all angular requests
        app.get('*', function(req, res) {
            res.sendfile('./public/index.html'); // load our public/index.html file
        });
        */

    };

