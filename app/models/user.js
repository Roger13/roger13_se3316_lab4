// app/models/nerd.js
// grab the mongoose module
var mongoose = require('mongoose');

module.exports = mongoose.model('User', {
    name : {type : String, default: ''},
    email : {type : String, default: ''},
    id : {type : String, default: ''},
    gender : {type : String, default: ''}
});