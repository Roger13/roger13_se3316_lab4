
    var clientId = '575376188422-717ivs16mq7tga01fdd2obpj43ufbvl2.apps.googleusercontent.com';
    var apiKey = 'AIzaSyAcYDLmsMxzvrByCzxaBNDYTFYG3iUrrDQ';
    var scopes = ["https://www.googleapis.com/auth/userinfo.profile", "https://www.googleapis.com/auth/userinfo.email"];
    
    // Use a button to handle authentication the first time.
    function handleClientLoad() {
        gapi.client.setApiKey(apiKey);
        window.setTimeout(checkAuth,1);
    }
    
    function checkAuth() {
        gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
    }
    
    
    function handleAuthResult(authResult) {
      var authorizeButton = document.getElementById('authorize-button');
      if (authResult && !authResult.error) {
        authorizeButton.style.visibility = 'hidden';
        makeApiCall();
      } else {
        authorizeButton.style.visibility = '';
        authorizeButton.onclick = handleAuthClick;
      }
    }

    
    function handleAuthClick(event) {
        gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
        return false;
    }
    
    // Load the API and make an API call.  Display the results on the screen.
    function makeApiCall() {
        gapi.client.load('plus', 'v1', function() {
          var request = gapi.client.plus.people.get({
            'userId': 'me'
          });
          request.execute(function(resp) {
            var heading = document.getElementById('user');
            var image = document.getElementById('profile');
            image.src = resp.image.url;
            image.style.visibility = '';
            
            var email = document.getElementById("email");
            var gender = document.getElementById("gender");
            var id = document.getElementById("id");
            
            email.appendChild(document.createTextNode(resp.emails[0].value));
            gender.appendChild(document.createTextNode(resp.gender));
            id.appendChild(document.createTextNode(resp.id));
            heading.appendChild(document.createTextNode(resp.displayName));

          });
        });
    }    