  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1528104227511073',
      xfbml      : true,
      version    : 'v2.5'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
   
  // Only works after `FB.init` is called
  function myFacebookLogin(msg) {
    FB.login(function(){
    // Note: The call will only work if you accept the permission request
    FB.api('/me/feed', 'post', {message: msg.previousSibling.innerHTML});
    }, {scope: 'publish_actions'});
  }