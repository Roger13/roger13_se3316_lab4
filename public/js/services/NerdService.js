angular.module('NerdService', []).factory('Nerd', ['$http', function($http) {

    return {
        // call to get all nerds
        get : function() {
            return $http.get('/api/nerds');
        },

        // call to POST and create a new post
        create : function(nerdData) {
            return $http.post('/api/nerds', nerdData);
        },
        
        // call to PUT and update a post
        update : function(nerdData, id) {
            return $http.put('/api/nerds/' + id, nerdData);
        },

        // call to DELETE a post
        delete : function(id) {
            return $http.delete('/api/nerds/' + id);
        },
        
        // call to GET all users
        getUsers : function() {
            return $http.get('/api/users');
        },
        
        // call to POST and create a new user
        createUser: function(userData) {
            return $http.post('/api/users', userData);
        }
    }       

}]);
