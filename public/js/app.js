var app =angular.module('sampleApp', ['ngRoute', 'appRoutes', 'MainCtrl', 'NerdCtrl', 'NerdService']);
app.controller('nerdDB', ['$scope', 'Nerd', function($scope, Nerd) {
    
    Nerd.get().then(function(msg) {
            $scope.posts = msg;
    });
    
    //Handles post creation
    this.post = "Edit your posts here";
    this.post2 = "Create your posts here";
    this.create = function create(post) {
        
        if (document.getElementById('user').innerHTML == "") {
            window.alert("Please, login first!");
            return;
        }
            
        Nerd.create({text: post}).then(function(msg) {
            Nerd.get().then(function(msg) {
                $scope.posts = msg;
                window.alert("Post created!");
            });
        });
    };
    
    //Handles post update
    this.update = function update(post, id) {
        if (document.getElementById('user').innerHTML == "") {
            window.alert("Please, login first!");
            return
        }
        
        Nerd.update({text: post}, id).then(function(msg) {
            Nerd.get().then(function(msg) {
                $scope.posts = msg;
                window.alert("Post updated!");
             });
        });
        
    };
    
    //Handles pos deletion
    this.del = function del(id) {
        if (document.getElementById('user').innerHTML == "") {
            window.alert("Please, login first!");
            return
        }
       
        Nerd.delete(id).then(function(msg) {
            Nerd.get().then(function(msg) {
                $scope.posts = msg;
                window.alert("Post deleted!");
             });
        });
    };
    
    //Handles user creation
    function createUser() {
        
        if (document.getElementById('user').innerHTML == "") {
            setTimeout(function() { createUser(); }, 1000);
            return;
        }
        
        Nerd.getUsers().then(function(msg) {
                $scope.users = msg;
            
                for (i in msg.data) {
                    if (msg.data[i].id == document.getElementById('id').innerHTML) {
                        window.alert("User already in the Database");
                        return;
                    }
                }
                
                window.alert("Thanks for logging in! Your information has been saved");
                Nerd.createUser({email: document.getElementById('email').innerHTML, id: document.getElementById('id').innerHTML, gender: document.getElementById('gender').innerHTML, name: document.getElementById('user').innerHTML}).then(function(msg) {
                    Nerd.getUsers().then(function(msg) {
                        $scope.users = msg;
                    });
                });
        });
    };
    
    setTimeout(function() { createUser(); }, 1000);
}]);
